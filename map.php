<?php

/**
 * PHP 7 script file.
 * Prototype to generating fixtures for yandex map hotspot layer.
 * @see https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/hotspot.ObjectSource-docpage/
 */

// not necessary parameter for this prototypes
// have to be escaped in case of usage
$zoom = $_GET['z'] ?? 0;
$x = $_GET['x'] ?? 0;
$y = $_GET['y'] ?? 0;

$features = [];

if ((int)$zoom === 3) {

    // use fixed value of hotspot size only for this prototype
    // real usage example have to calc based on given zoom and internal app settings
    for ($xAxis = 0; $xAxis < 256; $xAxis += 64) {

        for ($yAxis = 0; $yAxis < 256; $yAxis += 64) {
            $features[] = [
                'type' => 'Features',
                'properties' => [
                    'balloonContentBody' => 'Участник #' . implode('', [$x, $y, $xAxis, $yAxis]),
                    'HotspotMetaData' => [
                        'id' => (int)implode(['', $zoom, $x, $y, $xAxis, $yAxis]),
                        'RenderedGeometry' => [
                            'type' => 'Rectangle',
                            'coordinates' => [[$xAxis, $yAxis], [$xAxis + 64, $yAxis + 64]]
                        ]
                    ]
                ]
            ];
        }
    }
}

// build response data wrapper by yandex spec
$data = [
    'data' => [
        'type' => 'FeatureCollection',
        'features' => $features
    ]
];

// return jsonp callback

header('Content-Type: text/javascript; charset=utf8');
header('Access-Control-Allow-Origin: http://a0102022.xsph.ru');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$callback = $_GET['callback'];
echo $callback . '(' . json_encode($data) . ');';