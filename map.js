ymaps.ready(function () {

    var IMAGE_LAYER_NAME = 'image#layer',
        ICONS_LAYER_NAME = 'icons#layer',
        HOTSPOT_LAYER_NAME = 'hotspot#layer',
        MAP_TYPE_NAME = 'user#customMap',
        TILES_PATH = 'tiles',
        HOTSPOT_CALLBACK = 'http://localhost/map.php' // set url to callback here
        ;

    /**
     * Image layer constructor.
     */
    var imageLayer = function () {
        var layer = new ymaps.Layer(TILES_PATH + '/image_layer/%z/tile-%x-%y.jpg', {
            // define "not-found" tile
            notFoundTile: TILES_PATH + '/not-found.png'
        });

        // zoom ranges
        layer.getZoomRange = function () {
            return ymaps.vow.resolve([0, 3]);
        };

        // the method only to support legacy tiles
        // todo: must be removed after tiles will renamed
        layer.getTileUrl = function (tile, zoom) {

            // get tiles count by size, since we work with square it is right for every axis
            var tile_step = zoom ? Math.pow(2, zoom) * 3 : 3;
            tile_step++;

            var x = tile[0];
            var y = tile[1];

            if(x < 0 || y < 0 || x > (tile_step-1) || y > (tile_step-1)) {
                return null;
            }

            return this.getTileUrlTemplate().replace(/%x/i, x).replace(/%y/i, y).replace(/%z/i, zoom);
        };

        return layer;
    };

    /**
     * Active layer constructor.
     */
    var iconsLayer = function () {

        var layer = new ymaps.Layer(TILES_PATH + '/active_layer/%z/tile-%x-%y.png', {
            tileTransparent: true
        });

        // zoom ranges
        layer.getZoomRange = function () {
            return ymaps.vow.resolve([0, 3]);
        };

        // the method only to support legacy tiles
        // todo: must be removed after tiles will renamed
        layer.getTileUrl = function (tile, zoom) {

            // get tiles count by size, since we work with square it is right for every axis
            var tile_step = zoom ? Math.pow(2, zoom) * 3 : 3;
            tile_step++;

            var x = tile[0];
            var y = tile[1];

            if(x < 0 || y < 0 || x > (tile_step-1) || y > (tile_step-1)) {
                return null;
            }

            return this.getTileUrlTemplate().replace(/%x/i, x).replace(/%y/i, y).replace(/%z/i, zoom);
        };

        return layer;
    };

    /**
     * Hotspot layer constructor.
     */
    var hotspotLayer = function () {

        var objSource = new ymaps.hotspot.ObjectSource(HOTSPOT_CALLBACK + '?z=%z&x=%x&y=%y', 'callback_%c', {
            noCash: false // использовать кеш браузера
        });

        // check if request to server restricted
        objSource.restrict = function(layer, tileNumber, zoom) {
            // restrict except last zoom (3 in this example)
            return zoom != 3;
        };

        var layer = new ymaps.hotspot.Layer(objSource, {
            cursor: 'help',
            openEmptyHint: true,
            openEmptyBalloon: true,
            zIndex: 10000
        });

        layer.events.add('balloonopen', function (e) {
            console.log('balloon open');
        });

        layer.events.add('click', function (e) {
            // get clicked hotspot object
            var activeObject = e.get('activeObject');

            // TODO: find why the balloon doesn't opened on hotspot layer
            // var promised = layer.balloon.open(e.get('globalPixels'), activeObject._properties, layer.options);

            // In prototype we show balloon on the map layer, but in prod we have to use hotspot layer
            layer.getMap().balloon.open(e.get('coords'), activeObject._properties.balloonContentBody);

        }, this);

        return layer;
    };

    // add layers constructors to storage
    ymaps.layer.storage.add(IMAGE_LAYER_NAME, imageLayer);
    ymaps.layer.storage.add(ICONS_LAYER_NAME, iconsLayer);
    ymaps.layer.storage.add(HOTSPOT_LAYER_NAME, hotspotLayer);

    /**
     * Create a map type.
     */
    var mapType = new ymaps.MapType(MAP_TYPE_NAME, [IMAGE_LAYER_NAME, ICONS_LAYER_NAME, HOTSPOT_LAYER_NAME]);
    // Сохраняем тип в хранилище типов.
    ymaps.mapType.storage.add(MAP_TYPE_NAME, mapType);

    /**
     * Create a map.
     */
    var map = new ymaps.Map('map', {
        center: [2400, 2400],
        zoom: 0,
        controls: ['zoomControl'],
        type: MAP_TYPE_NAME
    }, {
        projection: new ymaps.projection.Cartesian([[0, 4800], [4800, 0]], [false, false])
    });

});